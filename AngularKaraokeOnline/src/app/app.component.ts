import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TokenInfo } from './models/token-info';
import { Video } from './models/video';
import { VideosInfo } from './models/videos-info';
import { YoutubeService } from './services/youtube-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild('videoPanel') videoPanel!: ElementRef;

  private readonly _router: Router;
  private readonly _youtubeService: YoutubeService;
  private readonly _tokenInfo: TokenInfo;
  private readonly _videosInfo: VideosInfo;

  searchedKeyword = '2k video';
  isLoadingVideo = false;
  readonly searchedVideos: Video[] = [];
  readonly columnWidthInPx = 300;
  readonly columnHeightInPx = 250;

  constructor(injector: Injector) {
    this._router = injector.get(Router);
    this._youtubeService = injector.get(YoutubeService);
    this._tokenInfo = injector.get(TokenInfo);
    this._videosInfo = injector.get(VideosInfo);
  }

  ngOnInit(): void {
    // for (let index = 0; index < 10; index++) {
    //   this.searchedVideos.push(new Video());  
    // }
  }

  async onLoadVideosClick() {
    if (this._tokenInfo.accessToken) {
      await this.loadVideos(true);
    }
    else {
      await this._router.navigate(['/login']);
    }
  }

  async onContainerScroll(event: Event) {
    const target = event.target as HTMLElement;
    if (!this.isLoadingVideo && (target.scrollLeft + target.offsetWidth >= target.scrollWidth)) {
      this.isLoadingVideo = true;
      await this.loadVideos(false);
      this.isLoadingVideo = false;
    }
  }

  onVideoClick(videoId: any) {
    console.log(videoId);
    
  }

  private async loadVideos(isClear: boolean) {
    if (isClear) {
      this._videosInfo.nextPageToken = '';
      this._videosInfo.prevPageToken = '';
      this._videosInfo.resultsPerPage = 0;
      this._videosInfo.totalResults = 0;
      this._videosInfo.items.clear();

      this.searchedVideos.length = 0;
    }

    const videos = await this._youtubeService.searchVideos(this.searchedKeyword, this._videosInfo.nextPageToken);
    this._videosInfo.nextPageToken = videos.nextPageToken;
    this._videosInfo.prevPageToken = videos.previousPageToken;
    this._videosInfo.resultsPerPage = videos.resultsPerPage;
    this._videosInfo.totalResults = videos.totalResults;
    this._videosInfo.items.set(videos.currentPageToken, videos.items);

    videos.items.forEach(x => {
      this.searchedVideos.push(x);
    });

    this.focusVideoPanel();
  }

  private focusVideoPanel() {
    this.videoPanel.nativeElement.focus();
  }
}
