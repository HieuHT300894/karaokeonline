import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { GoogleSettings } from '../models/google-settings';
import { Token } from '../models/token';
import { TokenInfo } from '../models/token-info';
import { Video } from '../models/video';
import { Videos } from '../models/videos';

@Injectable({
    providedIn: 'root'
})
export class YoutubeService {
    private readonly _httpClient: HttpClient;
    private readonly _googleSettings: GoogleSettings;
    private readonly _tokenInfo: TokenInfo;

    constructor(injector: Injector) {
        this._httpClient = injector.get(HttpClient);
        this._googleSettings = injector.get(GoogleSettings);
        this._tokenInfo = injector.get(TokenInfo);
    }

    async searchVideos(name='', pageToken=''): Promise<Videos> {
        const queryParams = new HttpParams()
            .set('part', 'snippet')
            .set('maxResults', 10)
            .set('videoEmbeddable', true)
            .set('type', 'video')
            .set('q', name)
            .set('pageToken', pageToken);
        
        pageToken = pageToken || '';
        if (!pageToken) {
            queryParams.delete('pageToken');
        }

        const options = {
            headers: {
                Authorization: [this._tokenInfo.tokenType, this._tokenInfo.accessToken].join(' ')
            },
            params: queryParams
        };

        const response = await lastValueFrom(this._httpClient.get<any>(this._googleSettings.youtubeSearchUrl, options));
        
        const videos = new Videos();
        videos.currentPageToken = pageToken || '';
        videos.nextPageToken = response['nextPageToken'] || '';
        videos.previousPageToken = response['prevPageToken'] || '';

        const pageInfo = response['pageInfo'];
        videos.totalResults = parseInt(pageInfo['totalResults']);
        videos.resultsPerPage = parseInt(pageInfo['resultsPerPage']);
        
        const items = response['items'] as any[];
        for(const item of items)
        {
            const video = new Video();
            video.id = item['id']['videoId'] || '';

            const snippet = item['snippet'];
            video.title = snippet['title'] || '';

            const thumbnail = snippet['thumbnails']['medium'];
            video.thumbnailUrl = thumbnail['url'] || '';
            video.thumbnailWidth = parseInt(thumbnail['width']);
            video.thumbnailHeight = parseInt(thumbnail['height']);

            videos.items.push(video);
        }

        return videos;
    }
};
