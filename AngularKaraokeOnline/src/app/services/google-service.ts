import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { Constants } from '../common/constants';
import { GoogleSettings } from '../models/google-settings';
import { Token } from '../models/token';

@Injectable({
    providedIn: 'root'
})
export class GoogleService {
    private readonly _httpClient: HttpClient;
    private readonly _googleSettings: GoogleSettings;

    constructor(injector: Injector) {
        this._httpClient = injector.get(HttpClient);
        this._googleSettings = injector.get(GoogleSettings);
    }

    async getToken(code: string): Promise<Token> {
        const queryParams = new HttpParams()
            .set('grant_type', this._googleSettings.grantTypeAuthorizationCode)
            .set('client_id', this._googleSettings.clientID)
            .set('client_secret', this._googleSettings.clientSecret)
            .set('redirect_uri', this._googleSettings.callbackUrl)
            .set('code', code);

        const response = await lastValueFrom(this._httpClient.post<any>(this._googleSettings.tokenUrl, queryParams));

        var token = new Token();
        token.expiresIn = parseInt(response['expires_in']) || 0;
        token.tokenType = response['token_type'] || '';
        token.accessToken = response['access_token'] || '';
        token.refreshToken = response['refresh_token'] || '';
        token.endToken = new Date(Date.now() + token.expiresIn * 1000);
        return token;
    }
};
