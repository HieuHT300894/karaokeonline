import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from 'src/app/common/constants';
import { GoogleSettings } from 'src/app/models/google-settings';
import { TokenInfo } from 'src/app/models/token-info';
import { GoogleService } from 'src/app/services/google-service';

@Component({
  selector: 'app-callback',
  template: ''
})
export class CallbackComponent implements OnInit {
  private readonly _route: ActivatedRoute;
  private readonly _googleService: GoogleService;
  private readonly _tokenInfo: TokenInfo;

  constructor(injector: Injector) { 
    this._route = injector.get(ActivatedRoute);
    this._googleService = injector.get(GoogleService);
    this._tokenInfo = injector.get(TokenInfo);
  }

  async ngOnInit() {
    try {
      const code = this._route.snapshot.queryParams["code"];
      if (code) {
        const token = await this._googleService.getToken(code);
        this._tokenInfo.tokenType = token.tokenType;
        this._tokenInfo.accessToken = token.accessToken;
        this._tokenInfo.refreshToken = token.refreshToken;
        this._tokenInfo.expiresIn = token.expiresIn;
        this._tokenInfo.endToken = token.endToken;
        
        localStorage.setItem(Constants.tokenName, JSON.stringify(token));
      }
    }
    catch {
      localStorage.removeItem(Constants.tokenName);
    }
    finally {
      window.location.href = '/login';
    }
  }
}
