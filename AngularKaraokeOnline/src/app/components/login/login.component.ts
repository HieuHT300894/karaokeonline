import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from 'src/app/common/constants';
import { Utils } from 'src/app/common/utils';
import { GoogleSettings } from 'src/app/models/google-settings';
import { Token } from 'src/app/models/token';
import { TokenInfo } from 'src/app/models/token-info';

@Component({
  selector: 'app-login',
  template: ''
})
export class LoginComponent implements OnInit {
  private readonly _router: Router;
  private readonly _googleSettings: GoogleSettings;
  private readonly _tokenInfo: TokenInfo;

  constructor(injector: Injector) {
    this._router = injector.get(Router);
    this._googleSettings = injector.get(GoogleSettings);
    this._tokenInfo = injector.get(TokenInfo);
  }

  ngOnInit(): void {
    this._tokenInfo.tokenType = '';
    this._tokenInfo.accessToken = '';
    this._tokenInfo.refreshToken = '';
    this._tokenInfo.expiresIn = 0;
    this._tokenInfo.endToken = new Date(Date.now() - 1);

    const tokenString = localStorage.getItem(Constants.tokenName);
    if (tokenString) {
      const token = JSON.parse(tokenString);

      this._tokenInfo.tokenType = token['tokenType'] || '';
      this._tokenInfo.accessToken = token['accessToken'] || '';
      this._tokenInfo.refreshToken = token['refreshToken'] || '';
      this._tokenInfo.expiresIn = parseInt(token['expiresIn']) || 0;
      this._tokenInfo.endToken = new Date(token['endToken']);
    }

    if (this._tokenInfo.endToken.getTime() >= Date.now()) {
      this._router.navigate(['/']);
      return;
    }

    localStorage.removeItem(Constants.tokenName);

    const queryStrings = new Map<string, string>();
    queryStrings.set('scope', this._googleSettings.scopes);
    queryStrings.set('access_type', this._googleSettings.accessType);
    queryStrings.set('include_granted_scopes', this._googleSettings.includeGrantedScopes);
    queryStrings.set('state', this._googleSettings.state);
    queryStrings.set('response_type', this._googleSettings.responseType);
    queryStrings.set('client_id', this._googleSettings.clientID);
    queryStrings.set('redirect_uri', this._googleSettings.callbackUrl);

    window.location.href = [this._googleSettings.authUrl, Utils.parseDictionaryToQuery(queryStrings)].join('?');
  }
}
