export class Video {
    id = 0;
    title = '';
    thumbnailUrl = '';
    thumbnailWidth = 0;
    thumbnailHeight = 0;
};
