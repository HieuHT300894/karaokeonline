export class Token {
    expiresIn = 0;
    tokenType = '';
    accessToken = '';
    refreshToken = '';
    endToken = new Date();
};
