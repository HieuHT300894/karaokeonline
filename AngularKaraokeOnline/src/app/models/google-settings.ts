import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class GoogleSettings {
    readonly authUrl = 'https://accounts.google.com/o/oauth2/v2/auth';
    readonly tokenUrl = 'https://www.googleapis.com/oauth2/v4/token';
    readonly tokenInfoUrl = 'https://www.googleapis.com/oauth2/v2/tokeninfo';
    readonly revokeUrl = 'https://accounts.google.com/o/oauth2/revoke';
    readonly youtubeSearchUrl = 'https://www.googleapis.com/youtube/v3/search';
    readonly scopes = 'https://www.googleapis.com/auth/youtube';
    readonly accessType = 'offline';
    readonly includeGrantedScopes = 'true';
    readonly state = 'state_parameter_passthrough_value';
    readonly responseType = 'code';
    readonly grantTypeAuthorizationCode = 'authorization_code';
    readonly grantTypeRefreshToken = 'refresh_token';
    readonly clientID = environment.google.clientID;
    readonly clientSecret = environment.google.clientSecret;
    readonly callbackUrl = environment.google.callbackUrl;
};
