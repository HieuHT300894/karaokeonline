import { Video } from "./video";

export class Videos {
    nextPageToken = '';
    previousPageToken = '';
    currentPageToken = '';
    totalResults = 0;
    resultsPerPage = 0;
    items: Video[] = [];
}