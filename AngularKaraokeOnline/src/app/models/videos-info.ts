import { Injectable } from "@angular/core";
import { Video } from "./video";

@Injectable({
    providedIn: 'root'
})
export class VideosInfo {
    nextPageToken = '';
    prevPageToken = '';
    totalResults = 0;
    resultsPerPage = 0;
    readonly items = new Map<string, Video[]>();
}