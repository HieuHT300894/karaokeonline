import { Injectable } from "@angular/core";
import { Token } from "./token";

@Injectable({
    providedIn: 'root'
})
export class TokenInfo {
    expiresIn = 0;
    tokenType = '';
    accessToken = '';
    refreshToken = '';
    endToken = new Date();
};
