export class Utils {
    static parseDictionaryToQuery(dictionary: Map<string, string>): string {
        if (!dictionary) {
            return '';
        }

        const keyValueStrings: string[] = [];

        dictionary.forEach((v, k) => keyValueStrings.push([k, v].join('=')));

        return keyValueStrings.join('&');
    }
}