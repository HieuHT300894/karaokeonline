﻿using KaraokeOnline.Common;
using KaraokeOnline.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using KaraokeOnline.Services;

namespace KaraokeOnline.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly YoutubeService _youtubeService;

        public IndexModel(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<ILogger<IndexModel>>();
            _youtubeService = serviceProvider.GetService<YoutubeService>();
        }

        public async Task OnGet()
        {
            var token = HttpContext.Session.GetObject<Token>(Constants.TokenName);
            if (token != null)
            {
                _logger.LogInformation(token.SerializeObjectToJson());

                var videos = await _youtubeService.SearchVideos(token, "2k video");
                _logger.LogInformation(videos.SerializeObjectToJson());

                var nextVideos = await _youtubeService.SearchVideos(token, "2k video", videos.NextPageToken);
                _logger.LogInformation(nextVideos.SerializeObjectToJson());

                var prevVideos = await _youtubeService.SearchVideos(token, "2k video", videos.PrevPageToken);
                _logger.LogInformation(prevVideos.SerializeObjectToJson());
            }
        }
    }
}
