using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Web;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using KaraokeOnline.Models;
using KaraokeOnline.Common;

namespace KaraokeOnline.Pages
{
    public class AuthenticationModel : PageModel
    {
        private readonly ILogger<AuthenticationModel> _logger;
        private readonly GoogleSettings _googleSettings;

        public AuthenticationModel(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<ILogger<AuthenticationModel>>();
            _googleSettings = serviceProvider.GetService<GoogleSettings>();
        }

        public IActionResult OnGet()
        {
            try
            {
                HttpContext.Session.Remove(Constants.TokenName);

                var builder = new UriBuilder();
                builder.Scheme = Request.Scheme;
                builder.Host = Request.Host.Host;
                builder.Port = Request.Host.Port.GetValueOrDefault();
                builder.Path = string.Join("/", "Callback");

                var query = HttpUtility.ParseQueryString(string.Empty);
                query["scope"] = _googleSettings.Scopes;
                query["access_type"] = _googleSettings.AccessType;
                query["include_granted_scopes"] = _googleSettings.IncludeGrantedScopes;
                query["state"] = _googleSettings.State;
                query["response_type"] = _googleSettings.ResponseType;
                query["client_id"] = _googleSettings.ClientID;
                query["redirect_uri"] = builder.ToString();

                return Redirect(string.Join("?", _googleSettings.AuthUrl, query));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "AuthenticationModel.OnGet");
                throw;
            }
        }
    }
}
