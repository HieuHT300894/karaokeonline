using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Web;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using KaraokeOnline.Models;
using KaraokeOnline.Common;
using Microsoft.AspNetCore.Http;

namespace KaraokeOnline.Pages
{
    public class CallbackModel : PageModel
    {
        private readonly ILogger<CallbackModel> _logger;
        private readonly GoogleSettings _googleSettings;

        public CallbackModel(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<ILogger<CallbackModel>>();
            _googleSettings = serviceProvider.GetService<GoogleSettings>();
        }

        public async Task<IActionResult> OnGet(string code)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(code))
                {
                    return RedirectToPage("Index");
                }

                var builder = new UriBuilder();
                builder.Scheme = Request.Scheme;
                builder.Host = Request.Host.Host;
                builder.Port = Request.Host.Port.GetValueOrDefault();
                builder.Path = string.Join("/", "Callback");

                var body = new Dictionary<string, string>();
                body.Add("grant_type", _googleSettings.GrantTypeAuthorizationCode);
                body.Add("client_id", _googleSettings.ClientID);
                body.Add("client_secret", _googleSettings.ClientSecret);
                body.Add("code", code);
                body.Add("redirect_uri", builder.ToString());

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_googleSettings.TokenUrl);
                    using (var request = new HttpRequestMessage(HttpMethod.Post, ""))
                    {
                        request.Content = new FormUrlEncodedContent(body);
                        using (var response = await client.SendAsync(request))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                var content = await response.Content.ReadAsStringAsync();
                                HttpContext.Session.SetObject(Constants.TokenName, content.DeserializeJsonToObject<Token>());
                            }
                        }
                    }
                }

                return RedirectToPage("Index");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "CallbackModel.OnGet");
                throw;
            }
        }
    }
}
