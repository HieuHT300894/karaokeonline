﻿using KaraokeOnline.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http.Headers;
using System.Web;
using Microsoft.AspNetCore.WebUtilities;
using KaraokeOnline.Common;
using Newtonsoft.Json.Linq;

namespace KaraokeOnline.Services
{
    public class YoutubeService
    {
        private readonly GoogleSettings _googleSettings;

        public YoutubeService(IServiceProvider serviceProvider)
        {
            _googleSettings = serviceProvider.GetService<GoogleSettings>();
        }

        public async Task<Videos> SearchVideos(Token token, string name, string pageToken = "")
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return new Videos();
            }

            var queryStrings = HttpUtility.ParseQueryString(string.Empty);
            queryStrings.Add("part", "snippet");
            queryStrings.Add("maxResults", "10");
            queryStrings.Add("videoEmbeddable", "true");
            queryStrings.Add("type", "video");

            if (string.IsNullOrWhiteSpace(pageToken))
            {
                queryStrings.Add("q", name);
            }
            else
            {
                queryStrings.Add("pageToken", pageToken);
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_googleSettings.YoutubeSearchUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.TokenType, token.AccessToken);

                using (var request = new HttpRequestMessage(HttpMethod.Get, string.Join("?", "", queryStrings)))
                {
                    using (var response = await client.SendAsync(request))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var content = await response.Content.ReadAsStringAsync();
                            var result = content.DeserializeJsonToObject<JToken>();
                            var items = result["items"] as JArray;

                            var videos = new Videos();
                            videos.NextPageToken = result["nextPageToken"]?.ToString();
                            videos.PrevPageToken = result["prevPageToken"]?.ToString();

                            var pageInfo = result["pageInfo"];
                            videos.TotalResults = Convert.ToInt32(pageInfo["totalResults"]);
                            videos.ResultsPerPage = Convert.ToInt32(pageInfo["resultsPerPage"]);

                            foreach (var item in items)
                            {
                                var video = new Video();
                                video.Id = item["id"]["videoId"]?.ToString();

                                var snippet = item["snippet"];
                                video.Title = snippet["title"]?.ToString();

                                var thumbnail = snippet["thumbnails"]["medium"];
                                video.ThumbnailUrl = thumbnail["url"]?.ToString();
                                video.ThumbnailWidth = Convert.ToInt32(thumbnail["width"]);
                                video.ThumbnailHeight = Convert.ToInt32(thumbnail["height"]);

                                videos.Items.Add(video);
                            }

                            return videos;
                        }
                    }
                }
            }

            return new Videos();
        }
    }
}
