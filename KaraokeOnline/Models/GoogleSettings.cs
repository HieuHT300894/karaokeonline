﻿using Microsoft.Extensions.Configuration;

namespace KaraokeOnline.Models
{
    public class GoogleSettings
    {
        public string AuthUrl { get; } = "https://accounts.google.com/o/oauth2/v2/auth";
        public string TokenUrl { get; } = "https://www.googleapis.com/oauth2/v4/token";
        public string TokenInfoUrl { get; } = "https://www.googleapis.com/oauth2/v2/tokeninfo";
        public string RevokeUrl { get; } = "https://accounts.google.com/o/oauth2/revoke";
        public string YoutubeSearchUrl { get; } = "https://www.googleapis.com/youtube/v3/search";
        public string Scopes { get; } = "https://www.googleapis.com/auth/youtube";
        public string AccessType { get; } = "offline";
        public string IncludeGrantedScopes { get; } = "true";
        public string State { get; } = "state_parameter_passthrough_value";
        public string ResponseType { get; } = "code";
        public string GrantTypeAuthorizationCode { get; } = "authorization_code";
        public string GrantTypeRefreshToken { get; } = "refresh_token";
        public string ClientID { get; } = string.Empty;
        public string ClientSecret { get; } = string.Empty;

        public GoogleSettings(IConfiguration configuration)
        {
            ClientID = configuration.GetValue("Google:ClientID", "");
            ClientSecret = configuration.GetValue("Google:ClientSecret", "");
        }
    }
}
