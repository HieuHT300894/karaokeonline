﻿using System.Collections.Generic;

namespace KaraokeOnline.Models
{
    public class Videos
    {
        public string NextPageToken { get; set; }
        public string PrevPageToken { get; set; }
        public long TotalResults { get; set; }
        public long ResultsPerPage { get; set; }
        public List<Video> Items { get; } = new List<Video>();
    }
}
