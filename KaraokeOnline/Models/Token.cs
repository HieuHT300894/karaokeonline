﻿using Newtonsoft.Json;
using System;

namespace KaraokeOnline.Models
{
    public class Token
    {
        public int expiresIn;

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn
        {
            get { return expiresIn; }
            set
            {
                expiresIn = value;
                EndToken = DateTime.Now.AddSeconds(value);
            }
        }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonIgnore]
        public DateTime EndToken { get; private set; }
    }
}
