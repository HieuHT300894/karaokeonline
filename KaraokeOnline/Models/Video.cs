﻿using Newtonsoft.Json;

namespace KaraokeOnline.Models
{
    public class Video
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string ThumbnailUrl { get; set; }
        public int ThumbnailWidth { get; set; }
        public int ThumbnailHeight { get; set; }
    }
}
