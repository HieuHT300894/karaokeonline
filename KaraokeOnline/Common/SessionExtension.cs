﻿using Microsoft.AspNetCore.Http;
using System.Text;

namespace KaraokeOnline.Common
{
    public static class SessionExtension
    {
        public static T GetObject<T>(this ISession session, string key)
        {
            var bytes = session.Get(key);
            if (bytes == null)
            {
                return default;
            }

            var value = Encoding.Default.GetString(bytes);
            if (string.IsNullOrWhiteSpace(value))
            {
                return default;
            }

            return value.DeserializeJsonToObject<T>();
        }

        public static void SetObject<T>(this ISession session, string key, T value)
        {
            session.Set(key, Encoding.Default.GetBytes(value.SerializeObjectToJson()));
        }
    }
}
