﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KaraokeOnline.Common
{
    public static class JsonExtension
    {
        public static string SerializeObjectToJson<T>(this T source)
        {
            var settings = new JsonSerializerSettings();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.DateFormatHandling = DateFormatHandling.MicrosoftDateFormat;

            return JsonConvert.SerializeObject(source, Formatting.Indented, settings);
        }

        public static T DeserializeJsonToObject<T>(this string source)
        {
            return JsonConvert.DeserializeObject<T>(source);
        }
    }
}
